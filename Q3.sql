﻿
create database qt default character set utf8;

use qt;

create table item_category(category_id int primary key auto_increment,category_name varchar(256) not null);

insert into item_category values ('1','家具');

insert into item_category values (2,'食品');

insert into item_category values (3,'本');

SELECT * FROM item_category;

