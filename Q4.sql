﻿
CREATE TABLE item(item_id INT PRIMARY KEY AUTO_INCREMENT,item_name VARCHAR(256) NOT NULL,item_price INT NOT NULL,category_id INT);

insert into item values (1,'堅牢な机',3000,1);
insert into item values (2,'生焼け肉',50,2);
insert into item values (3,'すっきりわかるJava入門',3000,3);
insert into item values (4,'おしゃれな椅子',2000,1);
insert into item values (5,'こんがり肉',500,2);
insert into item values (6,'書き方ドリルSQL',2500,3);
insert into item values (7,'ふわふわのベッド',30000,1);
insert into item values (8,'ミラノ風ドリア',300,2);

SELECT * FROM item;

